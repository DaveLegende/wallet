import 'package:flutter/material.dart';
import 'package:swiper/constantes/constants.dart';

class FillOutlinedButton extends StatelessWidget {
  const FillOutlinedButton({
    Key key,
    this.isFilled = true,
    @required this.press,
    @required this.text,
  }) : super(key: key);


  final bool isFilled;
  final VoidCallback press;
  final String text;
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
        side: BorderSide(color: appBarColor),
      ),
      color: isFilled ? appBarColor : Colors.transparent,
      onPressed: press,
      elevation: isFilled ? 4 : 0,
      child: Text(
        text,
        style: TextStyle(
          fontSize: 12,
          color: Colors.white,
        ),
      ),
    );
  }

}