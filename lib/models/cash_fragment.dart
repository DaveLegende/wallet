import 'package:flutter/material.dart';
import 'package:swiper/components/lists/list_users.dart';

class CashFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var long = MediaQuery.of(context).size.height * 0.2;
    ScrollController _controller = new ScrollController();

    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: Alignment.topCenter,
        overflow: Overflow.clip,
          children: [
            Positioned(
              top: 0,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "\$325.00",
                      textScaleFactor: 2,
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "TOTAL EARN",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                ),
              ),
            ),
            
            Positioned(
              top: long * 1.5,
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(top: 40, right: 20, left: 20),
                color: Colors.grey[200],
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text("PAYMENT HISTORY",
                            style:
                                TextStyle(color: Colors.grey[400], fontSize: 16)),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.white,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(5),
                            topRight: Radius.circular(5),
                          )),
                      child: ListView.builder(
                        controller: _controller,
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: 5,
                        itemBuilder: (context, index) => Column(
                          children: [
                            ListTile(
                              leading: ClipOval(
                                  child: Image(
                                image: AssetImage("${images[index]}"),
                                width: 40,
                                height: 40,
                                fit: BoxFit.cover,
                              )),
                              title: Text("${users[index]}",
                                  style: TextStyle(
                                      fontSize: 16, fontWeight: FontWeight.bold)),
                              subtitle: Text("${desc[index]}",
                                  style: TextStyle(
                                      color: Colors.grey[400], fontSize: 13)),
                              trailing: Text(trailing[index],
                                  style: TextStyle(
                                      fontSize: 16, fontWeight: FontWeight.bold)),
                            ),
                            Container(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: divider)
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            Positioned(
              top: long + 30,
              child: Container(
                padding: EdgeInsets.only(right: 10, left: 10),
                height: MediaQuery.of(context).size.height * 0.1,
                width: MediaQuery.of(context).size.width,
                child: Card(
                  elevation: 5,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            ClipOval(
                              child: Image(
                                image: AssetImage("assets/dollars.jpg"),
                                height: 45,
                                width: 45,
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(width: 20),
                            Text("Payment method"),
                          ],
                        ),
                        Icon(Icons.keyboard_arrow_right),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ]),
    );
  }
}
