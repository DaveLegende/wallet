import 'package:flutter/material.dart';
import 'package:swiper/constantes/constants.dart';
import 'package:swiper/models/cash_fragment.dart';
import 'package:swiper/models/discount_fragment.dart';

class PaymentPage extends StatefulWidget {

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  @override
  Widget build(BuildContext context) {

    var width = MediaQuery.of(context).size.width * 0.5;
    Widget fragment = CashFragment();
    bool onClick = false;

    return Scaffold(
      backgroundColor: mbSecondebleuColorCpfind,
      appBar: AppBar(
        backgroundColor: mbSecondebleuColorCpfind,
        title: Text(
          "My Wallet",
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        elevation: 0,
      ),
      drawer: Drawer(
        child: Container(color: mbSecondebleuColorCpfind),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
            children: [
              Expanded(
                child: InkWell(
                  onTap: (){
                    setState(() {
                      fragment = CashFragment();
                    });
                  },
                  child: Container(
                    width: width,
                    decoration: BoxDecoration(
                  color: appBarColor,
                  border: Border.all(
                    color: appBarColor,
                    width: 1,
                  ),
                  ),
                    padding: EdgeInsets.all(10),
                    child: Center(child: Text("Cash", style: TextStyle(color: Colors.white),)),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    setState(() {
                      fragment = DiscountFragment();
                    });
                  },
                  child: Container(
                    width: width,
                    decoration: BoxDecoration(
                  color: mbSecondebleuColorCpfind,
                  border: Border.all(
                    color: appBarColor,
                    width: 1,
                  ),
                  ),
                    padding: EdgeInsets.all(10),
                    child: Center(child: Text("Discount", style: TextStyle(color: Colors.white),)),
                  ),
                ),
              ),
            ],
          ),
          ),
          fragment,
        ]),
      ),
    );
  }
}
