import 'package:flutter/material.dart';

var users = [
  "Elva Bamett",
  "Isalah Francis",
  "Lula Riggs",
  "Ray Young",
  "Betty Palmer",
];

var desc = [
  "#740136",
  "#539642",
  "#123146",
  "#521936",
  "#129936",
];

var trailing = [
  "\$25.00",
  "\$12.00",
  "\$36.00",
  "\$33.00",
  "\$15.00",
];

var images = [
  "assets/img.jpg",
  "assets/img2.jpg",
  "assets/img3.jpg",
  "assets/img4.jpg",
  "assets/img5.jpg",
];

var divider = Divider(
    height: 1,
    thickness: 1,
);
