import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:swiper/screens/payment.dart';

import 'constantes/constants.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.width * 0.3;

    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: appBarColor,
          leading: IconButton(
            icon:
                Icon(Icons.keyboard_arrow_left, color: Colors.white, size: 30),
            onPressed: () {},
          ),
        ),
        body: SingleChildScrollView(
            child: Column(children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            width: MediaQuery.of(context).size.width,
            color: appBarColor,
            child: Container(
              padding: EdgeInsets.only(left: 10, right: 20),
              child: Align(
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("My Wallet",
                        textScaleFactor: 3,
                        style: TextStyle(
                            color: Colors.white, fontStyle: FontStyle.normal)),
                    Row(
                      children: [
                        ClipOval(
                          child: Image(
                            height: 20,
                            width: 20,
                            image: AssetImage("assets/boule.PNG"),
                            fit: BoxFit.cover,
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(right: 5)),
                        Text(
                          "2500",
                          textScaleFactor: 1.5,
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.25,
            color: appBarColor,
            child: Stack(
              children: [
                Column(children: [
                  Container(
                      height: MediaQuery.of(context).size.height * 0.125,
                      color: appBarColor),
                  Container(
                      height: MediaQuery.of(context).size.height * 0.125,
                      color: Colors.grey[200])
                ]),
                Container(
                  height: MediaQuery.of(context).size.height * 0.25,
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                    child: Swiper(
                      itemCount: 3,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          color: Colors.white,
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: InkWell(
                              onTap: () {
                                return Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            PaymentPage()));
                              },
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(bottom: 10),
                                    child: Row(
                                      children: [
                                        ClipOval(
                                          child: Image(
                                            image: AssetImage(
                                                "assets/dollars.jpg"),
                                            height: 45,
                                            width: 45,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        SizedBox(width: 20),
                                        Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text("Cash",
                                                  style: TextStyle(
                                                      color: appBarColor,
                                                      fontSize: 20,
                                                      fontStyle:
                                                          FontStyle.italic)),
                                              Text("Default Payment Method",
                                                  style: TextStyle(
                                                      color: Colors.grey[400],
                                                      fontSize: 15)),
                                            ]),
                                      ],
                                    ),
                                  ),
                                  Container(height: 1, color: Colors.grey[200]),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                      padding:
                                          EdgeInsets.only(left: 20, right: 20),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text("BALANCE",
                                                    style: TextStyle(
                                                        color: Colors.grey[400],
                                                        fontSize: 15)),
                                                Text("\$2500",
                                                    style: TextStyle(
                                                        color: Colors.red,
                                                        fontSize: 20)),
                                              ]),
                                          Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text("EXPIRES",
                                                    style: TextStyle(
                                                        color: Colors.grey[400],
                                                        fontSize: 15)),
                                                Text("09/21",
                                                    style: TextStyle(
                                                        color: appBarColor,
                                                        fontSize: 20)),
                                              ]),
                                        ],
                                      )),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                      viewportFraction: 0.8,
                      scale: 0.9,
                      pagination: SwiperPagination(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            color: Colors.grey[200],
            padding: EdgeInsets.only(top: 50),
            child: Column(
              children: [
                Container(
                  color: Colors.white,
                  child: Container(
                    padding: EdgeInsets.only(
                        right: 5, left: 10, top: 10, bottom: 10),
                    child: InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Payment Methods",
                            textScaleFactor: 1,
                          ),
                          Icon(Icons.keyboard_arrow_right),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(bottom: 15)),
                Container(
                  color: Colors.white,
                  child: Container(
                    padding: EdgeInsets.only(
                        right: 5, left: 10, top: 10, bottom: 10),
                    child: InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Coupon",
                            textScaleFactor: 1,
                          ),
                          Row(
                            children: [
                              Text(
                                "3",
                                textScaleFactor: 1,
                              ),
                              Padding(padding: EdgeInsets.only(right: 5)),
                              Icon(Icons.keyboard_arrow_right),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Container(
                    padding: EdgeInsets.only(
                        right: 5, left: 10, top: 10, bottom: 10),
                    child: InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Integral Mail",
                            textScaleFactor: 1,
                          ),
                          Row(
                            children: [
                              Text(
                                "2500",
                                textScaleFactor: 1,
                              ),
                              Padding(padding: EdgeInsets.only(right: 5)),
                              Icon(Icons.keyboard_arrow_right),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ])));
  }
}
